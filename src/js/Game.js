/* Основной объект, управляющий игрой */

var Game = {
    // Дублируем ENUM объекты в Game (костылёк для UI)
    ENUM: {
        OfferType: OfferType,
        AirplaneType: AirplaneType
    },

    // Доступные рейсы
    availableFlights: [],
    // Текущие рейсы игрока
    assignedFlights: [],

    // Список самолётов ???
    airplanes: [],

    logMessages: [],

    // Список аэропортов
    airports: ["Moscow DME", "Paris CDG", "Perm PEE", "Cairo CAI", "Madrid MAD", "Helsinki HEL", "Berlin BER", "Tokyo TYO", "New-York NYC", "Warsaw WAW"],
    // Расстояния между аэропортами
    distancesBetweenAirports: [
        [0],
        [2489, 0],
        [1157, 3610, 0],
        [2906, 3213, 3658, 0],
        [3444, 1054, 4592, 3354, 0],
        [893, 1910, 1789, 3386, 2952, 0],
        [1610, 878, 2736, 2895, 1871, 1106, 0],
        [7486, 9722, 6349, 9575, 10774, 7827, 8926, 0],
        [7518, 5844, 8116, 9032, 5774, 6626, 6392, 10861, 0],
        [1152, 1368, 2305, 2608, 2292, 916, 517, 8588, 6862, 0]
    ],

    getDistanceBetweenAirports: function (airportA, airportB) { //parameters are either number or name of airports
        for (var i = 0; i < Game.airports.length; i++) {
            if (Game.airports[i] == airportA) 
                airportA = i;
            if (Game.airports[i] == airportB)
                airportB = i;
        }
        airportA = +airportA;
        airportB = +airportB;
        if (isNaN(airportA) || isNaN(airportB))
            return 0;
        if (airportA > airportB)
            return Game.distancesBetweenAirports[airportA][airportB];
        else
            return Game.distancesBetweenAirports[airportB][airportA];
    },

    // Менеджер Событий
    eventManager: new EventManager(),

    // Текущий баланс игрока
    ballance: 2000000000,

    // Текущее игровое время в красивом формате для вывода на экран. Обновляется раз в секунду. КОСТЫЛЬ
    time: 0,

    // Текущие сделки
    shopOffers: [],

    // Объект-Магазин
    shop: Shop,    

    // Инициализация игры
    init: function() {
        // Инициализация менеджера событий и времени
        Game.eventManager.currentTime = new Date(2017, 0, 1, 0, 0, 0, 0);
        Game.time = Game.eventManager.currentTime;
        Game.eventManager.Start();
        // Загружаем предложения магазина из -файла- массива
        //Game.shop.LoadOffersFromFile('js/data/Airplanes.json');
        Game.shop.LoadOffersFromArray(ShopOffersData);

        //начать каждый день генерировать новые рейсы
        Game.eventManager.ScheduleEveryMorning(function() {
            Game.RemoveOldFlights();
            Game.GenerateDailyContracts();
            //console.log("genned");
        });

        Game.CalcMinOfferCost();
    },

    ProccessTransaction: function(transaction){},

    RemoveOldFlights: function() {
        var FlightsLen = Game.availableFlights.length;
        for (var i = FlightsLen - 1; i >= 0; i--) {
            var flight = Game.availableFlights[i];
            if (flight.daysShift == 0 || flight.daysShift == null || flight.daysShift == undefined) {
                if (flight.GetProgress() == -1 || flight.start == null)
                    Game.availableFlights.splice(i, 1);
            }
        }
    },

    GenerateDailyContracts: function() {
        //геним 3-7 рейса
        for (var iq = 0; iq < Utilities.RandInt(3, 7); iq++) {
            var flight = {};

            //генерируем пару различных аэропортов
            flight.airportA = Utilities.RandInt(0, Game.airports.length);
            flight.airportB = Utilities.RandInt(1, Game.airports.length);
            if (flight.airportB <= flight.airportA)
                flight.airportB--;

            //с вероятностью 0.4 рейс возвращается обратно
            flight.willReturn = (Math.random() < 0.4);

            //заполнение необходимой квалификации
            flight.targetQualification = new Qualification({
                //с вероятность 0.3 грузовой. с вероятностью 0.7 - пассажирский
                airplaneType: (Math.random() < 0.3 ? AirplaneType.Cargo : AirplaneType.Passenger),

                //длина рейса - длина между аэропортами, являющимися его концами
                maxFlightDistance: Game.getDistanceBetweenAirports(flight.airportA, flight.airportB),

                //кол-во пассажиров
                capacity: 0
            });


            //с вероятностью 0.2 рейс многоразовый
            if (Math.random() < 0.2) {
                //многоразовый рейс будет летать каждый 1-7 дней
                flight.daysShift = Utilities.RandInt(1, 7);
            } else {
                flight.daysShift = null;            
            }

            if (flight.targetQualification.airplaneType == AirplaneType.Passenger) {
                flight.profitability = 60 *
                    Math.pow(Game.getDistanceBetweenAirports(flight.airportA, flight.airportB), 2 / 3);
                flight.forfeit = flight.profitability * 15;
                flight.targetQualification.capacity =
                    (Math.random() < 0.5 ? Utilities.RandInt(50, 100) : Utilities.RandInt(100, 660));
            } else {
                flight.targetQualification.capacity = Utilities.RandInt(10, 120);
                flight.profitability = 600 * Math.pow(Game.getDistanceBetweenAirports(flight.airportA,
                            flight.airportB) * flight.targetQualification.capacity, 2 / 3);
                flight.forfeit = flight.profitability / 2;
            }

            flight.profitability = (flight.profitability / 100).toFixed(0) * 100;
            flight.forfeit = (flight.forfeit / 100).toFixed(0) * 100;

            Game.availableFlights.push(new Flight(flight));
        }
    },

    takeFlight: function(index) {
        if (index < 0 || index >= Game.availableFlights.length)
            return false;
        Game.assignedFlights.push(Game.availableFlights[index]);
        Game.availableFlights.splice(index, 1);
        return true;
    },

    isFlightRemovable: function(flight) {
        return (flight.GetProgress() == -1);
    },

    removeFlight: function(index) {
        if (index < 0 || index >= Game.assignedFlights.length)
            return false;
        if (!Game.isFlightRemovable(Game.assignedFlights[index]))
            return false;
        // Game.availableFlights.push(Game.assignedFlights[index]);
        Game.assignedFlights.splice(index, 1);
        return true;
    },

    removeFlightByElement: function(element) {
        var index = this.assignedFlights.indexOf(element);
        this.assignedFlights.splice(index, 1);
    },

    hideFlight: function(index) {
        if (index < 0 || index >= Game.availableFlights.length)
            return false;
        Game.availableFlights.splice(index, 1);
        return true;
    },

    makeEmptyFlight: function(shopOffer, airportA, airportB) {
        var flight = new Flight({
            airportA: airportA,
            airportB: airportB,
            willReturn: false,
            daysShift: null,
            forfeit: 0,
            targetQualification: shopOffer.airplane.qualification,
            assignedPlane: shopOffer.airplane,
            profitability: 0,
        });

        flight.assignedPlane = shopOffer.airplane;
        if (flight.PromlemsWithPlane())
            return false;
        
        flight.SetStartTime(new Date(Game.eventManager.currentTime.getTime() + 5000));

        this.assignedFlights.push(flight);
        return true;
    },

    minOfferCost: undefined,
    CalcMinOfferCost: function() {
        var min = Infinity;
        for (var offer in Shop.offers) {
            if (Shop.offers[offer].cost < min)
                min = Shop.offers[offer].cost;
        }
        Game.minOfferCost = min;
    },
    CheckLose: function() {
        var isActiveFlights = false;
        for (var flight in Game.assignedFlights)
            if (Game.assignedFlights[flight].GetProgress() != -1) {
                isActiveFlights = true;
                break;
            }
        return (Game.shopOffers.length > 0 || Game.ballance >= Game.minOfferCost || isActiveFlights);
    }
};

// Начинаем игру
Game.init();