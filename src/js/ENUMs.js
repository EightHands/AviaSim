/* Все ENUM объекты */

var OfferType = {
    Leasing: "Leasing",
    Rent: "Rent",
    Purchase: "Purchase"
};

var AirplaneType = {
    Passenger: "Passenger",
    Cargo: "Cargo"
};

Object.freeze(OfferType);
Object.freeze(AirplaneType);