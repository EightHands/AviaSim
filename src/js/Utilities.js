var Utilities = {
    RandInt: function(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    },
    BeautyNumber: function(s) {
        return Number(s).toString().replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1 ");
    },
    BeautyTime: function(time) {
        function addZero(x) {
            return (x < 10) ? "0" + x : x;
        }
        return addZero(time.getHours()) + ":" + addZero(time.getMinutes()) + " " + addZero(time.getDate()) + "." + addZero(time.getMonth() + 1) + "." + time.getFullYear();
    }
};