/* Class Airplane */

var Airplane = function(plane) {
    this.name = plane.name;
    this.maximumSpeed = plane.maximumSpeed;
    this.serviceCostPerKm = plane.serviceCostPerKm;
    this.qualification = plane.qualification;
    this.currentFlight = null;
    this.currentPosition = null;
    this.isInFlight = false;
};