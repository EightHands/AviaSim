var ShopOffersData = [
    
    new ShopOffer({
        airplane: new Airplane({
            name: "BAe Avro RJ70",
            maximumSpeed: 890,
            serviceCostPerKm: 6,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2715,
                capacity: 70
            })
        }),
        cost: 300000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "IL-114",
            maximumSpeed: 500,
            serviceCostPerKm: 9,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 1000,
                capacity: 64
            })
        }),
        cost: 400000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "ATR 72-200",
            maximumSpeed: 460,
            serviceCostPerKm: 16.4,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 1195,
                capacity: 74
            })
        }),
        cost: 820000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "Bombardier CRJ-1000",
            maximumSpeed: 830,
            serviceCostPerKm: 30,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2760,
                capacity: 100
            })
        }),
        cost: 1500000000,
        offerType: OfferType.Purchase
    }),
     new ShopOffer({
        airplane: new Airplane({
            name: "Embraer 195",
            maximumSpeed: 890,
            serviceCostPerKm: 32,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2650,
                capacity: 118
            })
        }),
        cost: 1600000000,
        offerType: OfferType.Purchase
    }),
     new ShopOffer({
        airplane: new Airplane({
            name: "Bombardier Dash 8 Q300",
            maximumSpeed: 530,
            serviceCostPerKm: 35,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 1180,
                capacity: 50
            })
        }),
        cost: 1750000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "TU-204C",
            maximumSpeed: 810,
            serviceCostPerKm: 35,
            qualification: new Qualification({
                airplaneType: AirplaneType.Cargo,
                maxFlightDistance: 6770,
                capacity: 27
            })
        }),
        cost: 1750000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "Sukhoi Superjet-100",
            maximumSpeed: 950,
            serviceCostPerKm: 35.4,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 3050,
                capacity: 98
            })
        }),
        cost: 1770000000,
        offerType: OfferType.Purchase
    }),

    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 757-300",
            maximumSpeed: 850,
            serviceCostPerKm: 80,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 6400,
                capacity: 279
            })
        }),
        cost: 4000000000,
        offerType: OfferType.Purchase
    }),
    
    new ShopOffer({
        airplane: new Airplane({
            name: "AN-124-100",
            maximumSpeed: 780,
            serviceCostPerKm: 150,
            qualification: new Qualification({
                airplaneType: AirplaneType.Cargo,
                maxFlightDistance: 13800,
                capacity: 120
            })
        }),
        cost: 4500000000,
        offerType: OfferType.Purchase
    }),
    
    
   
    
   
    
    
    
    
    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 747-400",
            maximumSpeed: 920,
            serviceCostPerKm: 228,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 13430,
                capacity: 660
            })
        }),
        cost: 11400000000,
        offerType: OfferType.Purchase
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 747-200F",
            maximumSpeed: 905,
            serviceCostPerKm: 238.8,
            qualification: new Qualification({
                airplaneType: AirplaneType.Cargo,
                maxFlightDistance: 9800,
                capacity: 112
            })
        }),
        cost: 11940000000,
        offerType: OfferType.Purchase
    }),
    
    
    
    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 787-10",
            maximumSpeed: 900,
            serviceCostPerKm: 306,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 11900,
                capacity: 440
            })
        }),
        cost: 15300000000,
        offerType: OfferType.Purchase
    }),
    
    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 777-300",
            maximumSpeed: 905,
            serviceCostPerKm: 330,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 11000,
                capacity: 550
            })
        }),
        cost: 16500000000,
        offerType: OfferType.Purchase
    }),
    
    
    new ShopOffer({
        airplane: new Airplane({
            name: "ATR 72-200",
            maximumSpeed: 460,
            serviceCostPerKm: 16.4,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 1195,
                capacity: 74
            })
        }),
        cost: 500000,
        offerType: OfferType.Rent
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "Douglas BC-9",
            maximumSpeed: 910,
            serviceCostPerKm: 41.5,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2200,
                capacity: 119
            })
        }),
        cost: 600000,
        offerType: OfferType.Rent
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "AN-148",
            maximumSpeed: 820,
            serviceCostPerKm: 30,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2100,
                capacity: 75
            })
        }),
        cost: 900000,
        offerType: OfferType.Rent
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "AN-74TK",
            maximumSpeed: 700,
            serviceCostPerKm: 30,
            qualification: new Qualification({
                airplaneType: AirplaneType.Cargo,
                maxFlightDistance: 4500,
                capacity: 10
            })
        }),
        cost: 900000,
        offerType: OfferType.Rent
    }),
    
    new ShopOffer({
        airplane: new Airplane({
            name: "Boeing 777-300",
            maximumSpeed: 905,
            serviceCostPerKm: 330,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 11000,
                capacity: 550
            })
        }),
        cost: 4435000,
        offerType: OfferType.Rent
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "BAe Avro RJ70",
            maximumSpeed: 890,
            serviceCostPerKm: 6,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2715,
                capacity: 70
            })
        }),
        cost: 16250000,
        offerType: OfferType.Leasing,
        leasingPeriodInDays: 20
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "Saab 2000",
            maximumSpeed: 560,
            serviceCostPerKm: 15,
            qualification: new Qualification({
                airplaneType: AirplaneType.Passenger,
                maxFlightDistance: 2100,
                capacity: 50
            })
        }),
        cost: 80000000,
        offerType: OfferType.Leasing,
        leasingPeriodInDays: 10
    }),
    new ShopOffer({
        airplane: new Airplane({
            name: "IL-96-400T",
            maximumSpeed: 850,
            serviceCostPerKm: 45,
            qualification: new Qualification({
                airplaneType: AirplaneType.Cargo,
                maxFlightDistance: 8460,
                capacity: 92
            })
        }),
        cost: 90000000,
        offerType: OfferType.Leasing,
        leasingPeriodInDays: 25
    })
];