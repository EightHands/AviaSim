/* Всё, что связано непосредственно с UI */

// VueApp - Наше приложение как vue сущность.
// Автоматически обновляются элементы на странице, связанные через объект Game
// В том числе большая часть обработки событий на странице
//Game.GenerateDailyContracts();

Vue.component('datepicker', {
    template: '<input v-bind:value="value" v-on:input="updateValue($event.target.value)" type="text"  />',
    props: ['value'],
    mounted: function() {
        var self = this;
        var dt = Game.eventManager.currentTime;
        function addZero(x) {
            return (x < 10) ? "0" + x : x;
        }
        this.$nextTick(function() {
            var pd = $(this.$el).pickadate({
                close: "",
                today: "",
                format: 'yyyy-mm-dd',
                formatSubmit: "yyyy-mm-dd",
                closeOnSelect: true,
                onSet: function(e) {
                    if (e.hasOwnProperty('select')) {
                        var newD = new Date(e.select);
                        console.log(newD);
                        self.updateValue(newD.getFullYear() + "-" + addZero(newD.getMonth() + 1) + "-" + addZero(newD.getDate()));
                    }
                    if (e.hasOwnProperty('clear'))
                        self.updateValue("");
                }
            });
            pd.pickadate('picker').set('select', [dt.getFullYear(), dt.getMonth(), dt.getDate()]);
            self.updateValue(dt.getFullYear() + "-" + addZero(dt.getMonth()+1) + "-" + addZero(dt.getDate()));
        });
    },
    methods: {
        updateValue: function (value) {
            this.$emit('input', value);
            this.$emit('change', value);
        },
    }
});

Game.animatedBallance = Game.ballance;
var VueApp = new Vue({
    // #App - элемент, в который обёрнута вся страница
    el: '#app',
    // Как только приложение загружено и собрано, убираем "Загрузка..."
    mounted: function(){
        $("#pageLoading").slideUp(500);
        this.flightForAssigning = undefined;
        this.emptyFlightTarget = null;
    }, 
    // Связка UI с главным объектом - Game
    data: Game,
    // Слежение за изменением свойств с перехватом этого изменения
    watch: {
        // Анимированное изменение балланса игрока при помощи Tween.js
        ballance: function(newValue, oldValue) {
            var vm = this;
            var animationFrame;
            function animate (time) {
                TWEEN.update(time);
                animationFrame = requestAnimationFrame(animate);
            }
            new TWEEN.Tween({tweeningNumber: oldValue})
                .easing(TWEEN.Easing.Quadratic.Out)
                .to({ tweeningNumber: newValue }, 1000)
                .onUpdate(function () {
                    vm.animatedBallance = this.tweeningNumber.toFixed(0)
                })
                .onComplete(function () {
                    cancelAnimationFrame(animationFrame)
                })
                .start();
            animationFrame = requestAnimationFrame(animate);
            if (!Game.CheckLose(newValue)) {
                $('.modal').modal();
                $('.modal').modal('open');
            }
        },
    },
    // Вычислимые атрибуты
    computed: {
        beautifulAnimatedBallane: function() {
            return Utilities.BeautyNumber(this.animatedBallance);
        }
        // beautifulTime: function(){
        //     var time = new Date(this.eventManager.currentTimeInMS;
        //     function addZero(x) {
        //         return (x < 10) ? "0" + x : x;
        //     }
        //     return addZero(time.getHours()) + ":" + addZero(time.getMinutes()) + " " + addZero(time.getDate()) + "." + addZero(time.getMonth()) + "." + time.getFullYear();
        // }
    },
    // Методы, используемые в UI
    methods: {
        // Устанавливает ускорение времени в Х раз
        timeX: function(x) {
            this.eventManager.timeScale = x;
        },
        // Возвращает вместимость самолёта с суфиксом (пасс или тонн)
        capacityWithSuffix: function(qualification) {
            return qualification.capacity.toString() + ((qualification.airplaneType === AirplaneType.Passenger) ? " пасс." : " тонн");
        },
        // Возвращает тип предложения на русском
        shopOfferInfo: function(shopOffer) {
            if (shopOffer.offerType === OfferType.Purchase) {
                return "Покупка";
            } else if (shopOffer.offerType === OfferType.Rent) {
                return "Аренда";
            } else if (shopOffer.offerType === OfferType.Leasing) {
                return "Лизинг";
            }
        },
        // Возвращает информацию о стоимости предложения в красивом виде
        shopOfferCostInfo: function(shopOffer) {
            if (shopOffer.offerType === OfferType.Purchase) {
                return Utilities.BeautyNumber(shopOffer.cost / 1000000) + " млн руб";
            } else if (shopOffer.offerType === OfferType.Rent) {
                return Utilities.BeautyNumber(shopOffer.cost) + " руб/день";
            } else if (shopOffer.offerType === OfferType.Leasing) {
                return Utilities.BeautyNumber(shopOffer.cost / 1000000) + " млн руб/день на " + shopOffer.leasingPeriodInDays + " дней";
            }
        },
        // Проверяет, можно ли заключить сделку
        shopOfferCanBeMade: function(offer) {
            return Shop.OfferCanBeMade(offer);
        },
        // Проверяет, можно ли разорвать сделку
        shopOfferCanBeBreaked: function(offer) {
            return Shop.OfferCanBeBreaked(offer);
        },
        // Заключение договора с магазином
        shopMakeOfferClick: function(shopOffer) {
            if (Shop.MakeOffer(shopOffer)) {
                var message;
                if (shopOffer.offerType === OfferType.Purchase) {
                    message = "Самолёт " + shopOffer.airplane.name + " куплен.";
                } else if (shopOffer.offerType === OfferType.Rent) {
                    message = "Самолёт " + shopOffer.airplane.name + " взят в аренду.";
                } else if (shopOffer.offerType === OfferType.Leasing) {
                    message = "Самолёт " + shopOffer.airplane.name + " взят в лизинг.";
                }
                UI.ShowMessage(message);
            } else {
                UI.ShowMessage("Операция не удалась... Проверьте балланс.");
            }
        },
        // Расторгнуть договор по индексу
        shopBreakOfferClick: function(index) {
            var message;
            if (Game.shopOffers[index].offerType === OfferType.Purchase) {
                message = "Самолёт " + Game.shopOffers[index].airplane.name + " продан.";
            } else if (Game.shopOffers[index].offerType === OfferType.Rent) {
                message = "Договор аренды самолёта " + Game.shopOffers[index].airplane.name + " расторгнут.";
            } else if (Game.shopOffers[index].offerType === OfferType.Leasing) {
                message = "Договор лизинга самолёта " + Game.shopOffers[index].airplane.name + " расторгнут.";
            }

            if (Shop.BreakOffer(index)) {
                UI.ShowMessage(message);
            } else {
                Materialize.toast("Операция не удалась... Возмонжно, самолёт сейчас используется...", 4000);
            }
        },
        // Проверяет, можно ли удалить рейс
        isFlightRemovable: function(flight) {
            return Game.isFlightRemovable(flight);
        },
        // Взять рейс
        takeFlightClick: function(index) {
            if (Game.takeFlight(index)) {
                UI.ShowMessage("Рейс взят");
            } else {
                UI.ShowMessage("Рейс не взят. Неизвестная ошибка");
            }
        },
        hideFlightClick: function(index) {
            if (Game.hideFlight(index)) {
                UI.ShowMessage("Рейс удалён из списка");
            } else {
                UI.ShowMessage("Рейс не удалён... Неизвестная ошибка...");
            }
        },
        // Удалить рейс
        removeFlightClick: function(index) {
            if (Game.removeFlight(index)) {
                UI.ShowMessage("Рейс удалён");
            } else {
                UI.ShowMessage("Данный рейс нельзя сейчас удалить");
            }
        },
        // Переход в режим выбора самолёта для рейса или открепление рейса
        assignFlightClick: function (flight){
            if (!flight.assignedPlane) {
                if (Game.shopOffers.length == 0) {
                    UI.ShowMessage("У вас нет ни одного самолёта...");
                    return;
                }
                this.flightForAssigning = flight;
                $("#mainTabsMenu").tabs('select_tab', 'shopOffers');
                $("#mainTabsMenu .tab").addClass("disabled");
                // go to tab and disable tabs
            } else {
                if (flight.UnAssign()) {
                    UI.ShowMessage("Самолёт откреплён");
                } else {
                    UI.ShowMessage("Самолёт невозможно сейчас открепить");
                }
            }
        },
        assignAirplaneClick: function(shopOffer){
            if (shopOffer != null) {
                if (this.flightForAssigning.Assign(shopOffer.airplane)) {
                    UI.ShowMessage("Самолёт прикреплён");
                } else {
                    UI.ShowMessage("Данный самолёт нельзя прикрепить на данный рейс");
                }
            }
            this.flightForAssigning = undefined;
            $("#mainTabsMenu .tab").removeClass("disabled");
            $("#mainTabsMenu").tabs('select_tab', 'assignedFlights');
        },
        flightTimeChanged: function(flight) {
            console.log(flight.temporaryDate);
            if (flight.temporaryTime != "" && flight.temporaryDate != "") {
                var dt = new Date(flight.temporaryDate + " " + flight.temporaryTime + ":00.00");
                if (flight.SetStartTime(dt)) {
                    UI.ShowMessage("Рейс добавлен в расписание");
                } else {
                    UI.ShowMessage("Вы не можете сейчас установить этот рейс на это время");
                }
            }
        },
        flightProgressUI: function(flight){
            return (flight.GetProgress() == -1) ? 0 : flight.GetProgress() * 100;
        },
        flightCancel: function(flight) {
            if (flight.Cancel()) {
                flight.temporaryTime = "";
                flight.temporaryDate = "";
                UI.ShowMessage("Рейс удалён из расписания");
            } else {
                UI.ShowMessage("Рейс нельзя сейчас удалить из расписания");
            }
        },
        makeEmptyFlightClick: function(shopOffer) {
            if (this.emptyFlightTarget == null)
                return;
            if (Game.makeEmptyFlight(shopOffer, shopOffer.airplane.currentPosition, this.emptyFlightTarget)) {
                UI.ShowMessage("Рейс для отправки самолёта создан");
                $('#mainTabsMenu').tabs('select_tab', 'assignedFlights');
            } else {
                UI.ShowMessage("Невозможно сейчас перегнать этот самолёт");
            }
        },
        beautyTime: function(time) {
            return Utilities.BeautyTime(time);
        }
    }
});

var UI = {
    ShowMessageDelay: 1000,
    ShowMessage: function(text){
        Materialize.toast(text, UI.ShowMessageDelay);
        Game.logMessages.push(new LogMessage(text));
    },
    ShowMessageMoney: function(amount, reason){
        Game.logMessages.push(new LogMessage(reason, amount));
        if (amount >= 0)
            Materialize.toast("Получено " + Utilities.BeautyNumber(amount) + " р. " + reason, UI.ShowMessageDelay);
        else
            Materialize.toast("Потрачено " + Utilities.BeautyNumber(-amount) + " р. " + reason, UI.ShowMessageDelay);
    },
};

$(function(){
    // Это не классика реактивного интерфейса, но на много эффективнее
    // Запускаем обновление отображаемого игрового времени 30 раз в секунду
    function updateTime(){
        var oldTime = new Date();
        function renderTime(){
            var currTime = new Date();
            if (currTime  - oldTime > 1000 / 30) {
                oldTime = currTime;
                Game.time = Utilities.BeautyTime(Game.eventManager.currentTime);
            }
            requestAnimationFrame(renderTime);
            return this;
        }
        renderTime();
    }
    updateTime();

    $('#mainTabsMenu').tabs({
        onShow: function(){
            $('select').material_select();
            // $('.datepicker').pickadate({
            //     close: "",
            //     format: 'dd.mm.yyyy',
            //     formatSubmit: "yyyy-mm-dd",
            //     closeOnSelect: true
            // });
        }
    });
});