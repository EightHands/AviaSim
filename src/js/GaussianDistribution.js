function GaussianDistribution (mean, standartDevivation) {
    /* usage example:
        var gaussianDistribution = new GaussianDistribution(34, 31);
        console.log(gaussianDistribution.getValue())
        console.log(gaussianDistribution.getValue(0.5)) //will return the mean value. Each call uses ~2500 iterations
    */


    this.mean = mean;
    this.standartDevivation = standartDevivation;
    

    this.erf = function (x) { //yet useless       
        // erf(x) = 2/sqrt(pi) * integrate(from=0, to=x, e^-(t^2) ) dt
        // with using Taylor expansion, 
        //        = 2/sqrt(pi) * sigma(n=0 to +inf, ((-1)^n * x^(2n+1))/(n! * (2n+1)))
        // calculationg n=0 to 50 bellow (note that inside sigma equals x when n = 0, and 50 may be enough)
        var m = 1.00;
        var s = 1.00;
        var sum = x * 1.0;
        for(var i = 1; i < 50; i++){
            m *= i;
            s *= -1;
            sum += (s * Math.pow(x, 2.0 * i + 1.0)) / (m * (2.0 * i + 1.0));
        }  
        return 2 * sum / Math.sqrt(Math.PI);
    }

    this.getValue = function(p) { //p belongs to [0; 1] or undefined
        if (p == undefined)
            return this.mean + this.standartDevivation * Math.sin(2 * Math.PI * (Math.random())) * Math.sqrt(-2 * Math.log(Math.random()));     

        var L = this.mean - 7 * this.standartDevivation;
        var R = this.mean + 7 * this.standartDevivation;
        var M;
        var value;
        for (var i = 0; i < 50; i++) {
            M = (L + R) / 2;
            value = 0.5 * (1 + this.erf((M - this.mean) / (this.standartDevivation * Math.sqrt(2))));
            //console.log(L + " " + M + " " + R + " " + value.toFixed(4));
            if (value < p)
                L = M;
            else
                R = M;
        }
        return (L + R) / 2;
    }
}