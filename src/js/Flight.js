/* Flight Class */

function Flight(flight) {
    this.airportA = flight.airportA;
    this.airportB = flight.airportB;
    this.willReturn = flight.willReturn;
    this.daysShift = flight.daysShift; //раз во сколько дней летает рейс (0 или null если рейс разовый)
    this.start = null;
    this.forfeit = flight.forfeit; 
    this.targetQualification = flight.targetQualification;
    this.assignedPlane = null;
    this.profitability = flight.profitability; //прибыльность рейса
    this.income = null;
    this.finalPassangers = null;
    // Костыль для UI
    this.temporaryTime = "";
    this.temporaryDate = "";

    this.CanAssign = function(airplane) {
        return airplane.qualification.IsSatisfies(this.targetQualification);
    };

    this.Assign = function(airplane) {
        if (!this.CanAssign(airplane))
            return false;

        this.assignedPlane = airplane;
        return true;
    };

    this.UnAssign = function() {
        this.assignedPlane = null;
        return true;
    };

    this.GetProgress = function() {
        if (this.PromlemsWithPlane())
            return -1;

        if (this.assignedPlane === null || this.assignedPlane === undefined)
            return -1;        

        var totalH = Game.getDistanceBetweenAirports(this.airportA, this.airportB) / this.assignedPlane.maximumSpeed;
        if (this.willReturn)
            totalH *= 2;

        var curH = (Game.eventManager.currentTime - this.start) / (3600 * 1000);

        if (curH < 0) 
            return -1;

        if (this.daysShift !== null && this.daysShift !== undefined && this.daysShift != 0) {
            var H_PER_CYCLE = 24 * this.daysShift;
            curH %= H_PER_CYCLE;
        }

        if (curH > totalH)
            return -1;

        return curH / totalH;            
    };
    
    this.getPassengerAmount = function() {
        if (this.finalPassangers !== null)
            return this.finalPassangers;
        if (this.targetQualification.airplaneType == AirplaneType.Cargo)
            return 0;
        var passengerNum = this.targetQualification.capacity;

        //летит произвольное число пассажиров от 0 до макс (обычно ближе к макс, чем к 0)
        passengerNum = Math.round(passengerNum * Math.sqrt(Math.random()));

        this.finalPassangers = passengerNum;

        //console.log("pas num = " + passengerNum);
        return passengerNum;
    };

    this.getFlightIncome = function() {
        if (this.income !== null) {
            //console.log("profitability = " + this.profitability + " income = " + this.income);
            return this.income;
        }

        if (this.targetQualification.airplaneType == AirplaneType.Cargo)
            return this.profitability;
        this.finalPassangers = this.getPassengerAmount();
        this.income = this.finalPassangers * this.profitability;

        //console.log("will return = " + this.willReturn);
        //console.log("profitability = " + this.profitability + " income = " + this.income);

        if (this.willReturn)
            this.income *= 2;

        //console.log("profitability = " + this.profitability + " income = " + this.income);

        return this.income;
    };

    
    this.getFlightCosts = function() {
        return Game.getDistanceBetweenAirports(this.airportA, this.airportB) * this.assignedPlane.serviceCostPerKm;
    }

    this.associatedEvent = null;
    this.watcherEvent = null;
    this.forfeitPayd = false;

    this.Test = function() {
        var date = new Date(Game.eventManager.currentTime.getTime() + 5 * 60 * 60 * 1000);
        console.warn(date);
        this.SetStartTime(date);
    }

    this.SetStartTime = function(startDate) {

        if (startDate < Game.eventManager.currentTime)
            return false;

        // Delete old event
        if (this.associatedEvent !== null)
            this.associatedEvent.shouldDispose = true;

        if (this.watcherEvent !== null)
            this.watcherEvent.shouldDispose = true;

        this.start = startDate;

        // TODO: more checks!!!
        // if (this.assignedPlane == null) {
        //     Game.ballance = Game.ballance - this.forfeit;
        // }

        // Create new event
        if (this.daysShift !== null)
        {
            var interval = Date.createTime(24 * this.daysShift, 0, 0);

            var self = this;
            this.associatedEvent = Game.eventManager.ScheduleWithInterval(interval, function() {

                if (self.PromlemsWithPlane())
                {
                    self.PayForfreit();
                    self.associatedEvent.shouldDispose  = true;
                    return;
                }

                var self2 = self;
                var started = false;
                self2.watcherEvent = Game.eventManager.ScheduleWithInterval(Date.createTime(0, 5, 0), function()
                {
                    if (started && self2.GetProgress() == -1) {
                        self2.Finished();
                        self2.watcherEvent.shouldDispose = true;
                    }
                    else if (!started && self2.GetProgress() != -1) {
                        started = true;
                        self2.Started();
                    }                                        
                });
            }, this.start);
        }
        else
        {
            var self = this;
            this.associatedEvent = Game.eventManager.ScheduleExactTime(this.start, function() {
                var self2 = self;

                if (self.PromlemsWithPlane())
                {
                    self.PayForfreit();
                    self.associatedEvent.shouldDispose = true;
                    return;
                }

                var started = false;
                self2.watcherEvent = Game.eventManager.ScheduleWithInterval(Date.createTime(0, 5, 0), function()
                {
                    if (started && self2.GetProgress() == -1) {
                        self2.Finished();
                        self2.watcherEvent.shouldDispose = true;
                    }
                    else if (!started && self2.GetProgress() != -1) {
                        started = true;
                        self2.Started();
                    }                                        
                });
            });
        }

        return true;
    };

    this.Finished = function() {
        if (this.daysShift === null)  {
            this.assignedPlane.currentPosition = this.airportB;
            Game.removeFlightByElement(this);
        }
        else {
            this.assignedPlane.currentPosition = this.airportA;
        }

        this.assignedPlane.currentFlight = null;

        Game.ballance = Game.ballance + this.getFlightIncome();
        UI.ShowMessage("Рейс из " + Game.airports[this.airportA] + " до " + Game.airports[this.airportB] + " окончен.");
        UI.ShowMessageMoney(this.getFlightIncome(), "Вознаграждение за рейс.");
    }

    this.Started = function() {
        this.assignedPlane.currentFlight = this;
        Game.ballance = Game.ballance - this.getFlightCosts();
        UI.ShowMessage("Рейс из " + Game.airports[this.airportA] + " до " + Game.airports[this.airportB] + " начат.");
        if (this.targetQualification.airplaneType == AirplaneType.Passenger)
            UI.ShowMessage("На рейс село " + this.getPassengerAmount() + " пассажира(ов).");
        UI.ShowMessageMoney(-this.getFlightCosts(), "Затраты на рейс.");
    }
    
    this.PayForfreit = function () {
        if (this.forfeitPayd == false)
        {
            Game.ballance = Game.ballance - this.forfeit;
            UI.ShowMessageMoney(-this.forfeit, "Выплата неустойки по рейсу из " + Game.airports[this.airportA] + " до " + Game.airports[this.airportB] + ".");
            this.forfeitPayd = true;
        }
    }

    this.Cancel = function() {
        if (this.associatedEvent !== null)
            this.associatedEvent.shouldDispose = true;
        
        if (this.watcherEvent != null)
            this.watcherEvent.shouldDispose = true;

        this.start = null;

        this.PayForfreit();
        this.forfeitPayd = false;

        return true;
    }

    this.PromlemsWithPlane = function() {
        return this.assignedPlane === null ||
              (this.assignedPlane.currentFlight !== null && this.assignedPlane.currentFlight !== this) ||
              (this.assignedPlane.currentPosition !== null && this.assignedPlane.currentPosition != this.airportA);
    }
}