// Взаимодействие со счетом клиента происходит при помощи транзакций.
function Transaction(amount, description = "") {
    this.amount = amount;
    this.description = description;
}