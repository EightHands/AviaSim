/* Shop Object */
/* Объект магазин. Отвечает за операции купле-продажи, аренды, лизинга */

var Shop = {
    // Сделки, которые предлагает магазин
    offers: [],

    // Коэффициент продажи. Определяет, за сколько будет продан самолёт относительно стоимости покупки
    sellCoeff: 1,

    // Загружает данные о сделках из файла
    /*
    Не работает локально в Chrome по причине проблем с безопасностью
    LoadOffersFromFile: function(file) {
        var offers = [];
        $.getJSON(file, "", function(data) {
            //offersData = $.parseJSON(data);
            for (var i in data) {
                var offer = {};
                for (var key in data[i]) {
                    if (key == "airplane") {
                        var airplane = {};
                        for (var airplaneKey in data[i][key]) {
                            if (airplaneKey == "qualification") {
                                airplane[airplaneKey] = new Qualification(data[i][key][airplaneKey]);
                            } else {
                                airplane[airplaneKey] = data[i][key][airplaneKey];
                            }
                        }
                        offer[key] = new Airplane(airplane);
                    } else {
                        offer[key] = data[i][key];
                    }
                }
                offers.push(new ShopOffer(offer));
            }
            Shop.offers = offers;
        });
    },
    */
    LoadOffersFromArray: function(offersArray) {
        Shop.offers = offersArray;
    },

    // Можно ли совершить сделку
    OfferCanBeMade: function(offer) {
        return Game.ballance >= offer.cost;
    },
    // Совершить сделку (купить самолёт / взять в аренду / в лизинг)
    MakeOffer: function(offer) {
        //создаём новый объект, чтобы при изменении одного самолёта не менялись все такие же
        offer = new ShopOffer(offer);
        offer.airplane = new Airplane(offer.airplane);
        offer.airplane.qualification = new Qualification(offer.airplane.qualification);

        // Проверяем, можно ли совершить сделку
        if (!Shop.OfferCanBeMade(offer))
            return false;

        // Совершаем сделку в зависимости от типа сделки
        switch (offer.offerType) {
            case OfferType.Purchase:
                return Shop.Buy(offer);
            case OfferType.Rent:
                return Shop.Rent(offer);
            case OfferType.Leasing:
                return Shop.Leasing(offer);
            default:
                return false;
        }
    },
    // Покупка самолёта
    Buy: function(offer) {
        // Transaction
        Game.ballance -= offer.cost;
        UI.ShowMessageMoney(-offer.cost, "Покупка самолета");
        Game.shopOffers.push(offer);
        return true;
    },
    // Аренда самолёта
    Rent: function(offer) {
        Game.shopOffers.push(offer);
        //EventManager.ScheduleWithInterval
        offer.event = Game.eventManager.ScheduleWithInterval(Date.createTime(24,0,0), function() {
            if (Game.ballance < offer.cost) {
                this.shouldDispose = true;
                console.log("Not enough money. Offer will be disturbed");
                Shop.BreakOffer(Game.shopOffers.indexOf(offer));
            } else {
                Game.ballance -= offer.cost;
                UI.ShowMessageMoney(-offer.cost, "Аренда самолета");
            }
            //console.log("Снято за аренду: " + offer.cost);
        }, Game.eventManager.currentTime);
        return true;
    },
    // Лизинг Самолёта
    Leasing: function(offer) {
        //console.log("lizing: " + offer.leasingPeriodInDays);
        Game.shopOffers.push(offer);
        //console.log(offer);
        offer.event = Game.eventManager.ScheduleWithIntervalExactTimes(Date.createTime(24,0,0), function() {
            if (Game.ballance < offer.cost) {
                this.shouldDispose = true;
                console.log("Not enough money. Offer will be disturbed");
                Shop.BreakOffer(Game.shopOffers.indexOf(offer));
            } else {
                Game.ballance -= offer.cost;
                UI.ShowMessageMoney(-offer.cost, "Лизинг самолета");
            }
        }, offer.leasingPeriodInDays, Game.eventManager.currentTime);
        return true;
    },


    // TODO
    // Можно ли разорвать договор
    OfferCanBeBreaked: function(offer) {
        // // Продавать можно только самолёты, не прикреплённые
        if (offer.event != undefined && offer.event.shouldDispose)
            return true;
        if (offer.airplane.currentFlight == null || offer.airplane.currentFlight == undefined)
            return true;
        return (offer.airplane.currentFlight.GetProgress() == -1);
    },
    // Отменить договор покупки
    BreakOffer: function(index) {
        // Проверяем, можно ли продать самолёт
        if (!Shop.OfferCanBeBreaked(Game.shopOffers[index]))
            return false;
        // Расторгаем договор в зависимости от типа договора
        switch (Game.shopOffers[index].offerType) {
            case OfferType.Purchase:
                return Shop.Sell(index);
            case OfferType.Rent:
                return Shop.BreakRentOrLeasing(index);
            case OfferType.Leasing:
                return Shop.BreakRentOrLeasing(index);
            default:
                return false;
        }
    },
    // Продать самолёт игрока
    Sell: function(index) {
        // Transaction
        Game.ballance += Game.shopOffers[index].cost * Shop.sellCoeff;
        UI.ShowMessageMoney(Game.shopOffers[index].cost * Shop.sellCoeff, "Продажа самолета");
        Game.shopOffers.splice(index, 1);

        return true;
    },
    BreakRentOrLeasing: function(index) {
        Game.shopOffers[index].event.shouldDispose = true;
        Game.shopOffers.splice(index, 1);
        return true;
    },
};