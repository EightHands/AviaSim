function Qualification(qualification) {
    this.airplaneType = qualification.airplaneType;
    this.maxFlightDistance = qualification.maxFlightDistance;
    this.capacity = qualification.capacity;

    // Пример использования: airplaneQualification.IsSatisfies(flightQualification);
    this.IsSatisfies = function(other) {
        return this.airplaneType == other.airplaneType &&
               this.maxFlightDistance >= other.maxFlightDistance &&
               this.capacity >= other.capacity;
    }
}