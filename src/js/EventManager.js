///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Вспомогательные расширения класса Date
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Date.createTime = function(hours, minutes, seconds) {
    if (hours == undefined) hours = 0;
    if (minutes == undefined) minutes = 0;
    if (seconds == undefined) seconds = 0;
    var time = (3600 * hours + 60 * minutes + seconds) * 1000;
    return new Date(time);
};

Date.prototype.AddSeconds = function(count) {
    this.setTime(this.getTime() + count * 1000);
    return this;
};

Date.prototype.AddMinutes = function(count) {
    this.AddSeconds(60 * count);
    return this;
};

Date.prototype.AddHours = function(count) {
    this.AddMinutes(60 * count);
    return this;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// EventManager и его вспомогательные классы
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Пример использования:
    // var eventManager = new EventManager();
    // eventManager.Start();
    // eventManager.ScheduleInTime(Date.createTime(5, 0, 0), function() {
    //     console.log("5 hours left!");
    // });
    // eventManager.ScheduleEveryMorning(function() {
    //     console.log("Good morning!");
    // });

function EventManager()
{
    /// Текущее игровое время
    this.currentTime = new Date();
    // Использовалось для рактивной работы с UI, но лсишком ресурсозатратно
    // this.currentTimeInMS = +this.currentTime;

    /// Шаг игрового времени в миллисекнудах
    this.timeScale = 1;
    this.timeScaleToRealLife = 200;

    this.events = [];

    this.lastProcessedTime = this.currentTime;

    /// Планирует событие через определенный интервал игрового времени, например через час.
    /// TODO: Придумать нормальное название
    this.ScheduleInTime = function(time, callback) {
        var newTime = new Date(this.currentTime.getTime() + time.getTime());
        return this.ScheduleExactTime(newTime, callback);
    };

    /// Планирует событие на конкретное игровое время
    this.ScheduleExactTime = function(time, callback) {
        var newEvent = new SingleEvent(time, callback);
        this.events.push(newEvent);
        return newEvent;
    };

    this.ScheduleEveryMorning = function(callback) {
        var newEvent = new PeriodicEvent(new Date(this.currentTime.getTime()), Date.createTime(24,0,0), callback);
        this.events.push(newEvent);
        return newEvent;
    };

    this.ScheduleWithInterval = function(interval, callback, startFrom) {
        if (startFrom == undefined)
            startFrom = this.currentTime;
        var newEvent = new PeriodicEvent(new Date(startFrom.getTime()), interval, callback);
        this.events.push(newEvent);
        return newEvent;
    };

    this.ScheduleWithIntervalUntilTime = function(interval, callback, duration_in_MS, startFrom) {
        if (startFrom == undefined)
            startFrom = this.currentTime;
        var newEvent = new PeriodicEvent(new Date(startFrom.getTime()), interval, callback,
            new Date(startFrom.getTime() + duration_in_MS));
        this.events.push(newEvent);
        return newEvent;
    };

    this.ScheduleWithIntervalExactTimes = function(interval, callback, iterationsNum, startFrom) {
        if (startFrom == undefined)
            startFrom = this.currentTime;
        var newEvent = new PeriodicEvent(new Date(startFrom.getTime()), interval, callback, null, iterationsNum);
        this.events.push(newEvent);
        return newEvent;
    };

    this.Start = function() {
        var self = this;
        requestAnimationFrame(function() {self.ProcessTimeStep(); });
    };

    this.addZero = function(x) {
        return (x < 10) ? "0" + x : x;
    };

    this.ProcessTimeStep = function() {
        var curTime = Date.now();
        var dateDiff = curTime - this.lastProcessedTime;
        this.lastProcessedTime = curTime;

        this.currentTime.setTime(this.currentTime.getTime() + dateDiff * this.timeScale * this.timeScaleToRealLife);

        // Использовалось для рактивной работы с UI, но лсишком ресурсозатратно
        // this.currentTimeInMS = +this.currentTime;

        var i;

        for (i = 0; i < this.events.length; i++) {
            var event = this.events[i];
            event.Update(this.currentTime);
        }

        var event_len = this.events.length;
        for (i = event_len - 1; i >= 0; i--)
            if (this.events[i].shouldDispose)
                this.events.splice(i, 1);

        var self = this;
        requestAnimationFrame(function() {self.ProcessTimeStep(); });
    }
}

function SingleEvent(elapseTime, callback) {
    this.elapseTime = elapseTime;
    this.callback = callback;

    this.shouldDispose = false;
    this.Update = function(currentTime) {
        var timeBeforeEvent = this.elapseTime - currentTime;
        if (timeBeforeEvent <= 0) {
            this.callback();
            this.shouldDispose = true;
        }
    }
}

function PeriodicEvent(startFrom, interval, callback, endTime, iterationsLeft) {
    this.startFrom = startFrom;
    this.interval = interval;
    this.callback = callback;

    this.endTime = endTime;
    this.iterationsLeft = (iterationsLeft == null || iterationsLeft == undefined ? -1 : iterationsLeft);

    this.shouldDispose = false;
    this.Update = function(currentTime) {
        var timeBeforeEvent = this.startFrom - currentTime;
        if (timeBeforeEvent <= 0) {
            this.callback();
            this.iterationsLeft--;
            if (this.iterationsLeft == 0)
                this.shouldDispose = true;
            this.startFrom.setTime(this.startFrom.getTime() + this.interval.getTime());
            if (this.endTime != undefined && this.endTime != null && this.endTime.getTime() <= this.startFrom.getTime())
                this.shouldDispose = true;
        }
    }
}

/*var num = 0;
function kek() {
    var em = new EventManager();
    em.Start();
    //em.ScheduleWithIntervalExactTimes(Date.createTime(0,20,0), function() {console.log("kek" + num++);}, 12);
    em.ScheduleWithIntervalUntilTime(Date.createTime(0,20,0), function() {console.log("kek"+num++);}, 1000*3600*4);
}*/